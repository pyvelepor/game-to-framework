//variables for drawing the game
var viewport = document.getElementById("viewport");
var context = viewport.getContext('2d');

//Background sprite
var backgroundImage = new Image();
backgroundImage.src = "map.png";
backgroundImage.onload = (event) => {
    context.drawImage(backgroundImage, 0, 0, backgroundImage.width, backgroundImage.height);
};

//Variables for character sprite
var x=32;
var y=32;
var dx=0;
var dy=0;
var speed = 1;
var direction ='right';
var playerImage = new Image();
playerImage.src = "player.png";
playerImage.onload = (event) => {
    context.drawImage(playerImage, x, y, playerImage.width, playerImage.height);
};

//Variables for character's energy blast sprite
var blast = false;
var blastX=-16;
var blastY=-16;
var blastDx=0
var blastDy=0;
var blastDirection = 'right';
var drawBlast = false;
var blastImage = new Image();
blastImage.src = "blast.png";
blastImage.onload = (event) => {
    context.drawImage(blastImage, -16, -16, blastImage.width, blastImage.height);    
};

//Variables for monster sprite
var monsterX=256;
var monsterY=16;
var monsterDx=0;
var monsterDy=0;
var monsterSpeed=0.5;
var monsterImage = new Image();
monsterImage.src = "monster.png";
monsterImage.onload = (event) => {
    context.drawImage(monsterImage, x, y, monsterImage.width, monsterImage.height);
};

//Variables for monster's energy  blast sprite
var monsterBlastX=0;
var monsterBlastY=0;
var monsterBlastDx=0;
var monsterBlastDy=0;
var monsterBlastSpeed=1.5;
var monsterBlastDirection="down";
var drawMonsterBlast = false;
var monsterBlastImage = new Image();
monsterBlastImage.src = "monster-blast.png";
monsterBlastImage.onload = (event) => {
    context.drawImage(monsterBlastImage, monsterBlastX, monsterBlastY, monsterBlastImage.width, monsterBlastImage.height);
};

//variables for keyboard input
var left = false;
var right = false;
var up = false;
var down = false;
var space = false;

/*
    Function handles the following:
        (1) updates the position of the character and other game objects
        (2) checks for collisions between character and other sprites
        (3) draws everything
*/
var draw = () => {
    //Reset change in position for all sprites
    dx = 0;
    dy = 0;
    blastDx = 0;
    blastDy = 0;
    monsterDx = 0;
    monsterDy = 0;
    monsterBlastDx = 0;
    monsterBlastDy = 0;
    
    //Update character
    if(left === true){
        direction = "left";
        dx -= speed;
    }
    
    else if(right === true){
        direction = "right";
        dx += speed;
    }
    
    else if(up === true){
        direction = "up";
        dy -= speed;
    }
    
    else if(down === true){
        direction = "down";
        dy += speed;
    }
    
    //Stop character from walking through left or right trees
    if((x + dx < 16) || (x + 16 + dx > (viewport.width - 16))){
        dx = 0;
    }
    
    //Stop character from walking through top or bottom trees
    if((y + dy < 16) || (y + 16 + dy > (viewport.height - 16))){
        dy = 0;
    }
    
    //Stops character from walking through monster
    if((x + 16 + dx >= monsterX) &&
       (x + dx < (monsterX + 16)) &&
       (y + 16 + dy >= monsterY) &&
       (y + dy < (monsterY + 16))){
           dx = 0;
           dy = 0;
    }
    
    //Update characters position
    x += dx;
    y += dy;
    
    //Update character's energy blast
    
    //Limits us to only firing one energy blast.
    //Can't fire a new one until the old one has hit something
    if(blast === true && drawBlast === false){
        drawBlast = true;
        
        //Direction of the energy blast needs to be the same
        //direction thath the character is facing
        blastDirection = direction;
        
        //Set the x and y position based off the direction
        if(blastDirection === "down"){
            blastX = x;
            blastY = y + 16;
        }
        
        else if(blastDirection === "up"){
            blastX = x;
            blastY = y - 16;
        }
        
        else if(blastDirection === "left"){
            blastX = x -16;
            blastY = y;
        }
        
        else if(blastDirection === "right"){
            blastX = x + 16;
            blastY = y;
        }
    }
    
    //If statemetns set the speed of the energy blast based off the direciton
    if(blastDirection === "down"){
        blastDy = 3 * speed;
    }
    
    else if(blastDirection === "up"){
        blastDy = -3 * speed;
    }
    
    else if(blastDirection === "left"){
        blastDx = -3 * speed;
    }
    
    else if(blastDirection === "right"){
        blastDx = 3 * speed;
    }

    //Stops energy blast from travelling through trees
    if((blastX + blastDx < 16) ||
       (blastX + 8 + blastDx >= (viewport.width - 16)) ||
       (blastY + blastDy < 16) ||
       (blastY + 8 + blastDy >= (viewport.width - 16))){
       drawBlast = false;
       blastDx = 0;
       blastDy = 0;
    }
    
    //Update position of energy blast
    blastX += blastDx;
    blastY += blastDy;
    
    //Update monster
    monsterDy = monsterSpeed;
    
    //Stop monster from walking through trees
    if((monsterX + monsterDx < 16) ||
       (monsterX + 16 + monsterDx >= (viewport.width - 16)) ||
       (monsterY + monsterDy < 0) ||
       (monsterY + 16 + monsterDy >= (viewport.height - 16))){
        monsterX = Math.random() * ((viewport.width - 16) - 16) + 16;
        monsterY = 16;
        monsterDy = 0;
    }
    
    //Redraw monster if it walks into character
    if((monsterX + 16 + monsterDx >= x) &&
       (monsterX + monsterDx < (x + 16)) &&
       (monsterY + 16 + monsterDy >= y) &&
       (monsterY + monsterDy < (y + 16))){
        monsterX = Math.random() * ((viewport.width - 16) - 16) + 16;
        monsterDy = 0;
        monsterY = 16;
    }
    
    //Redraw monster if it walks into character's energy blast
    if((monsterX + 16 + monsterDx >= blastX) &&
       (monsterX + monsterDx < (blastX + 8)) &&
       (monsterY + 16 + monsterDy >= blastY) &&
       (monsterY + monsterDy < (blastY + 8))){
        monsterX = Math.random() * ((viewport.width - 16) - 16) + 16;
        monsterDy = 0;
        monsterY = 16;
        drawBlast = false;
    }

    //Update monsters y position
    //X position doesn't change, so it doesn't need to be updated
    monsterY += monsterDy;
    
    //Update monster's energy blast
    if(drawMonsterBlast === false){
        shouldFire = Math.random();
        
        /*
            Controls how often the monster fires.
            Values between 0.99 and 0.999 work well.
            0.99: Fire all the time
            0.999: Almost never fire
        */
        if(shouldFire > 0.995){
            drawMonsterBlast = true;
            monsterBlastY = monsterY + 16;
            monsterBlastX = monsterX;
            monsterBlastDx = 0;
        }
    }
    
    //Update monster energy blast
    monsterBlastDy = monsterBlastSpeed;
    
    //Stop monster's energy blast from travelling through trees
    if((monsterBlastX + monsterBlastDx < 16) ||
       (monsterBlastX + 8 + monsterBlastDx >= (viewport.width - 16)) ||
       (monsterBlastY + monsterBlastDy < 16) ||
       (monsterBlastY + 8 + monsterBlastDy >= (viewport.height - 16))){
        monsterBlastX = monsterX;
        monsterBlastY = monsterY;
        monsterBlastDy = 0;
        drawMonsterBlast = false;
    }
    
    //Stop monster's energy blast from travelling through character
    if((monsterBlastX + 8 + monsterBlastDx >= x) &&
       (monsterBlastX + monsterBlastDx < (x + 16)) &&
       (monsterBlastY + 8 + monsterBlastDy >= y) &&
       (monsterBlastY + monsterBlastDy < (y + 16))){
           monsterBlastX = monsterX;
           monsterBlastY = monsterY;
           monsterBlastDy = 0;
           drawMonsterBlast = false;
    }
    
    //update postion for monster's energy blast
    monsterBlastX += monsterBlastDx;
    monsterBlastY += monsterBlastDy;
    
    /*
        Draw all the sprites, in this order.
    
        (1) Background
        (2) Monster
        (3) Character
        (4) Character's energy blast
        (5) Monster's energy blast
    */
    context.drawImage(backgroundImage, 0, 0, backgroundImage.width, backgroundImage.height);
    context.drawImage(monsterImage, monsterX, monsterY, monsterImage.width, monsterImage.height);
    context.drawImage(playerImage, x, y, playerImage.width, playerImage.height);
    
    if(drawBlast === true){
        context.drawImage(blastImage, blastX, blastY, blastImage.width, blastImage.height);
    }
    
    if(drawMonsterBlast === true){
        context.drawImage(monsterBlastImage, monsterBlastX, monsterBlastY, monsterBlastImage.width, monsterBlastImage.height);
    }
    
    /*
        Creates a loop by calling `draw`. Allows us to update sprites for next frame.
    */
    window.requestAnimationFrame(draw);
};

//chekcs if a key has been released
var onKeyUp = (event) => {
    /*
      `WASD` for moving
       w: up
       d: right
       s: down
       a: up
       space: blast
       
      keycodes
      a: keyA
      d: keyD
      s: keyS
      w: keyW
      space: Space
    */
    
    if(event.code === 'KeyA'){
        left = false;
    }
    
    else if(event.code === 'KeyD'){
        right = false;
    }
    
    else if(event.code === 'KeyS'){
        down = false;
    }
    
    else if(event.code === 'KeyW'){
        up = false;
    }
    
    else if(event.code === "Space"){
        blast = false;
    }
};

//checks if a key has been pressed
var onKeyDown = (event) => {
    /*
      `WASD` for moving
       w: up
       d: right
       s: down
       a: up
       space: blast
       
      keycodes
      a: keyA
      d: keyD
      s: keyS
      w: keyW
      space: Space
    */
    
    if(event.code === 'KeyA'){
        left = true;
    }
    
    else if(event.code === 'KeyD'){
        right = true;
    }
    
    else if(event.code === 'KeyS'){
        down = true;
    }
    
    else if(event.code === 'KeyW'){
        up = true;
    }
    
    else if(event.code === "Space"){
        blast = true;
    }
};

/*
    Code for setting up the game. 
    
    (1) Tell the browser to call `onKeyDown` when a key is down or pressed
    (2) Tell the browser to call `onKeyUp` when a key is  up released.
    (3) Start the game by calling `draw`. Draw uses `requestAnimationFrame`, which
        loops by repeatedly calling draw.
*/
document.addEventListener('keydown', onKeyDown);
document.addEventListener('keyup', onKeyUp);
draw();
