Example of creating a game framework. Starts with a really basic concept and builds up to something more complex.

"Tiny 16: Basic" by Lanea Zimmerman licensed [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/) (The anti-DRM clause of CC-BY 3.0 is waved), [OGA-BY 3.0](http://static.opengameart.org/OGA-BY-3.0.txt): [https://opengameart.org/content/tiny-16-basic]().

![Screenshot of game](screenshot.png)
